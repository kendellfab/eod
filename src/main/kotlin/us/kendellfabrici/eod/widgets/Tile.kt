package us.kendellfabrici.eod.widgets

import javafx.scene.input.MouseButton
import javafx.scene.layout.StackPane
import javafx.scene.paint.Color
import javafx.scene.shape.Rectangle
import javafx.scene.text.Text
import org.kordamp.ikonli.javafx.FontIcon
import us.kendellfabrici.eod.listeners.TileSelected

class Tile(val col: Int, val row: Int, val hasBomb: Boolean, val tileSize: Double, val tileSelected: TileSelected): StackPane() {

    val border: Rectangle = Rectangle(tileSize - 2, tileSize - 2)
    val hint = Text()
    val icon = FontIcon()
    var isOpen = false
    var isFlagged = false
    val iconSize = (tileSize / 2).toInt()

    var proximalBombs: Int = 0

    init {
        border.stroke = Color.LIGHTGRAY
        border.fill = Color.DARKGREY

        children.addAll(border, hint, icon)
        hint.isVisible = false
        icon.isVisible = false

        if(hasBomb) {
            setBomb()
        }

        translateX = col * tileSize
        translateY = row * tileSize

        setOnMouseClicked {
            if(it.button == MouseButton.SECONDARY) {
                if(isFlagged) {
                    if(hasBomb) {
                        setBomb()
                        icon.isVisible = false
                    } else {
//                        icon.iconLiteral = ""
                        icon.isVisible = false
                    }
                    tileSelected.unFlagged(this)
                } else {
                    icon.iconLiteral = "fas-flag-checkered:$iconSize"
                    icon.iconColor = Color.ORANGERED
                    icon.isVisible = true
                    tileSelected.flagged(this)
                }

                isFlagged = !isFlagged
            } else {
                if(isOpen || isFlagged) {
                    return@setOnMouseClicked
                }
                border.fill = Color.TRANSPARENT
                if (hasBomb) {
                    icon.isVisible = true
                    tileSelected.clickedBomb(this)
                } else {
                    hint.isVisible = true
                    if (proximalBombs == 0) {
                        tileSelected.clickedEmpty(this)
                    } else {
                        tileSelected.clickedRegular(this)
                    }
                }
                isOpen = true
            }
        }
    }

    fun setBomb() {
        icon.iconLiteral = "fas-bomb:$iconSize"
        icon.iconColor = Color.DARKRED
    }

    fun open() {
        isOpen = true
        border.fill = Color.TRANSPARENT
        if(hasBomb) {
            icon.isVisible = true
        } else {
            hint.isVisible = true
        }
    }

    fun isEmptySpace(): Boolean {
        return proximalBombs == 0
    }

    fun updateProximalBombs(prox: Int) {
        proximalBombs = prox

        if(prox > 0) {
            val color = when (prox) {
                1 -> Color.GREEN
                2 -> Color.BLUE
                3 -> Color.PURPLE
                else -> Color.RED
            }
            hint.text = proximalBombs.toString()
            hint.fill = color
        }
    }
}