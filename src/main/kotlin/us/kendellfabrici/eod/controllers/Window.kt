package us.kendellfabrici.eod.controllers

import javafx.fxml.FXML
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.layout.Pane
import us.kendellfabrici.eod.listeners.TileSelected
import us.kendellfabrici.eod.widgets.Tile

class Window: TileSelected {
    override fun flagged(t: Tile) {
        flagged++
        updateCounts()
    }

    override fun unFlagged(t: Tile) {
        flagged--
        updateCounts()
    }

    override fun clickedBomb(t: Tile) {
        for(c in pnBoard.children) {
            if(c is Tile) {
                c.setOnMouseClicked {  }
                if(c.hasBomb) {
                    c.open()
                }
            }
        }
    }

    override fun clickedEmpty(t: Tile) {
        checkEmptyNeighbors(t)
    }

    override fun clickedRegular(t: Tile) {

    }

    fun checkEmptyNeighbors(t: Tile) {
        if(t.isOpen) {
            return
        }
        t.open()
        if(t.isEmptySpace()) {
            getNeighbors(t).forEach { t -> checkEmptyNeighbors(t) }
        }
    }

    @FXML
    lateinit var btNewGame: Button
    @FXML
    lateinit var pnBoard: Pane
    @FXML
    lateinit var lblCounts: Label

    private var grid: Array<Array<Tile>>? = null
    private var bombCount = 0
    private var flagged = 0

    @FXML
    fun handleNewGame() {
        println("New Game Selected")
        setupBoard()
    }

    @FXML
    fun initialize() {
        setupBoard()
    }

    var cols = 0.0
    var rows = 0.0

    private fun setupBoard() {
        val w = 700
        val h = 450

        cols = w / TILE_SIZE
        rows = h / TILE_SIZE

        bombCount = 0
        flagged = 0
        var tempGrid: Array<Array<Tile>> = arrayOf()
        pnBoard.children.clear()

        for(row in 0 until rows.toInt()) {
            var rowArr = arrayOf<Tile>()
            for(col in 0 until cols.toInt()) {
                val isBomb = Math.random() < 0.5
                if(isBomb) {
                    bombCount++
                }
                val tile = Tile(col, row, isBomb, TILE_SIZE, this)
                rowArr += tile
                pnBoard.children.add(tile)
            }
            tempGrid += rowArr
        }
        grid = tempGrid
//        printBoard()

        for(row in 0 until rows.toInt()) {
            for(col in 0 until cols.toInt()) {
                val t = grid!![row][col]
                val ns = getNeighbors(t)
                t.updateProximalBombs(bombCount(ns))
            }
        }
//        printBoard()

        updateCounts()
    }

    fun updateCounts() {
        lblCounts.text = "$flagged/$bombCount"
    }

    fun printBoard() {
        println("*** Board ***")
        for(arr in grid!!.iterator()) {
            for(item in arr.iterator()) {
                val out = if(item.hasBomb) "*" else item.proximalBombs
                print(out)
            }
            println()
        }
    }

    fun bombCount(ts: Array<Tile>): Int {
        var count = 0
        for(t in ts) {
            if(t.hasBomb) {
                count++
            }
        }
        return count
    }

    fun getNeighbors(tile: Tile): Array<Tile> {
        var neighbors: Array<Tile> = arrayOf()

        val points: IntArray = intArrayOf(
            -1, -1,
            -1, 0,
            -1, 1,
            0, -1,
            0, 1,
            1, -1,
            1, 0,
            1, 1
        )

        for(i in 0 until points.size step 2) {
            val dx = points[i]
            val dy = points[i+1]

            val newX = tile.row + dx
            val newY = tile.col + dy

            if(newX in 0..(rows.toInt() - 1) && newY in 0..(cols.toInt() - 1)) {
                val n = grid!![newX][newY]
                neighbors += n
            }
        }

        return neighbors
    }

    companion object {
        const val TILE_SIZE = 25.0
    }
}