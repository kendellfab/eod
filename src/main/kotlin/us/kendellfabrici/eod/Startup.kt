package us.kendellfabrici.eod

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage

class Startup: Application() {
    override fun start(primaryStage: Stage?) {
        val loader = FXMLLoader(javaClass.getResource("/fxml/window.fxml"))
        val root = loader.load<Parent>()

        val scene = Scene(root)

        primaryStage?.scene = scene
        primaryStage?.isResizable = false
        primaryStage?.title = "E.O.D."
        primaryStage?.show()
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Application.launch(Startup::class.java, *args)
        }
    }
}