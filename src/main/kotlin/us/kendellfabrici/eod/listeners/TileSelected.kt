package us.kendellfabrici.eod.listeners

import us.kendellfabrici.eod.widgets.Tile

interface TileSelected {
    fun clickedBomb(t: Tile)
    fun clickedEmpty(t: Tile)
    fun clickedRegular(t: Tile)
    fun flagged(t: Tile)
    fun unFlagged(t: Tile)
}